#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){
    pid_t pid, sid;
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    FILE *fp;

    if(strcmp(argv[1], "-a") == 0){
        fp = fopen("killer.sh", "w");
        fprintf(fp, "killall -SIGKILL lukisan; rm killer.sh;\n");
        fclose(fp);
    }

    if(strcmp(argv[1], "-b") == 0){
        fp = fopen("killer.sh", "w");
        fprintf(fp, "kill -9 %d; rm killer.sh\n", (int)getpid());
        fclose(fp);
    }

    pid = fork();
    if(pid == 0){
        char *argvmod[] = {"chmod", "u+x", "killer.sh", NULL};
        execv("/bin/chmod", argvmod);
    }
    while((wait(NULL)) != pid);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        char ZipName[64], CurrentTime[30], link[50];
        pid_t cid;
        int status;
        
        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        char FolderName[64];
        strftime(FolderName, 64, "%Y-%m-%d_%H:%M:%S", tm);
        
        cid = fork();

        if(cid < 0){
            exit(EXIT_FAILURE);
        }

        if(cid == 0){
            if(fork()==0){
                char *argvmkdir[] = {"mkdir", "-p", FolderName, NULL};
                execv("/bin/mkdir", argvmkdir);
            }
            else{
                while((wait(&status)) > 0);
                for(int i=0 ; i<15 ; i++){
                    if(fork()==0){
                        if(chdir(FolderName) < 0){
                            exit(EXIT_FAILURE);
                        }
                        time_t t2 = time(NULL);
                        struct tm* p2 = localtime(&t2);
                        strftime(CurrentTime, 30, "%Y-%m-%d_%H:%M:%S", p2);
                        sprintf(link, "https://picsum.photos/%ld", (t2 % 1000) + 50);

                        char *argvdown[] = {"wget", link, "-O", CurrentTime, "-o", "/dev/null", NULL};
                        execv("/usr/bin/wget", argvdown);
                    }
                    sleep(5);
                }
                sprintf(ZipName, "%s.zip", FolderName);

                char *argvzip[] = {"zip", "-qrm", ZipName, FolderName, NULL};
                execv("/usr/bin/zip", argvzip);
            }
        }
        sleep(30);
    }
}

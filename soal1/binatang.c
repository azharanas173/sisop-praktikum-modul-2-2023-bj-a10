#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int main(){

    // ==DOWNLOAD===================================

    pid_t download_id;
    int status_download;

    download_id = fork();

    char link[] = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";

    if (download_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (download_id == 0) {
        char *argv[] = {"wget", link, "-qO", "binatang.zip", NULL};
        execv("/bin/wget", argv);
    }
    wait(&status_download);

    // ==UNZIP===================================

    pid_t unzip_id;
    int status_unzip;

    unzip_id = fork();

    if (unzip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (unzip_id == 0) {
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }
    wait(&status_unzip);

    // ==PEMILIHAN===================================

    char basePath[] = ".";
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    char *path[11];

    if (!dir)
        return 0;
    
    int i = 0;
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            if (dp->d_type == DT_REG) {
                char *hewan  = dp->d_name;
                path[i] = hewan;
                i++;
            }
        }
        // printf("%ld -> %s -> %ld -> %d -> %d\n", dp->d_ino, dp->d_name, dp->d_off, dp->d_reclen, dp->d_type);
    }
    int pilih = rand() % sizeof(*path);

    FILE *f = fopen("jaga.txt", "w");
    fprintf(f, "Penjagaan pada hewan %s", strtok(path[pilih], "."));
    fclose(f);

    closedir(dir);

    // ==GRUP===================================

    pid_t grup_id;
    int status_grup;

    grup_id = fork();

    if (grup_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (grup_id == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
    }
    wait(&status_grup);

    // ==PEMILAHAN===================================
    
    DIR *dirr = opendir(".");
    pid_t pilah_id;
    int status_pilah;

    while ((dp = readdir(dirr)) != NULL) {
                
        if (dp->d_type == DT_REG) {
            if (strstr(dp->d_name, "darat") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanDarat", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }

            if (strstr(dp->d_name, "amphibi") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAmphibi", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }

            if (strstr(dp->d_name, "air") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAir", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }
        }
    }
    closedir(dirr);

    // ==ZIPZIPan===================================

    pid_t zip_id;
    int status_zip;

    // 1. Darat
    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

    // 2. Amphibi
    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

    // 3. Air
    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

    // ==DELETE===================================
    
    pid_t delete_id;
    int status_delete;

    delete_id = fork();

    if (delete_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (delete_id == 0) {
        char *argv[] = {"rm", "-r", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/rm", argv);
    }
    wait(&status_delete);
}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>	
#include <sys/types.h>
#include <wait.h>

int main(){

    // ==DOWNLOAD===================================
    pid_t download;
    int status_download;
    char path[500] = "/home/players";
    download = fork();

  char link[] = "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";

    if (download < 0) {
        exit(EXIT_FAILURE);
    }

    if (download == 0) {
        char *argv[] = {"wget", link, "-qO", "players.zip", NULL};
        execv("/bin/wget", argv);
    }
	wait(&status_download);
    

    // ==UNZIP=================
    pid_t unzip;
    int status_unzip;

    unzip = fork();

    if (unzip < 0) {
        exit(EXIT_FAILURE);
    }

    if (unzip == 0) {
        char *argv[] = {"unzip", "-q", "players.zip", NULL};
        execv("/bin/unzip", argv);
    }
	wait(&status_unzip);

// ==DELETE=================
    pid_t delete_zip;
    int status_delete;

    delete_zip = fork();

    if (delete_zip< 0) {
        exit(EXIT_FAILURE);
    }

    if (delete_zip== 0) {
        char *argv[] = {"rm", "-r", "players.zip", NULL};
        execv("/bin/rm", argv);
    }
     wait(&status_delete);
   
    // Remove non-ManUtd files
    DIR *dir = opendir("players");
    if (!dir) {
        perror("Directory open error");
        exit(1);
    }

    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
            // Ignore . and .. directories
            continue;
        }

        // Check if file name contains "ManUtd"
        if (strstr(dp->d_name, "ManUtd") == NULL) {
            char filepath[500];
            sprintf(filepath, "players/%s", dp->d_name);
            remove(filepath);
        }
    }
    closedir(dir);
    
   // ==Folder=======================
    pid_t folder_id;
    int status_folder;

    folder_id = fork();

    if (folder_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (folder_id == 0) {
        char *argv[] = {"touch", "Kiper", "Bek", "Gelandang", "Penyerang", NULL};
        execvp("touch", argv);
    }
    wait(&status_folder);
    
    
    // === urutan ====
	 DIR *dir2 = opendir(".");
	 pid_t urutan;
	 int status_urutan;
	 
	 while((dp = readdir(dir2)) != NULL){

		 if(strstr(dp->d_name, "Kiper") != NULL){
	 		urutan = fork();
	 
	 		if (urutan < 0){
	 			exit(EXIT_FAILURE);
	 		}
 	
 	 		if (urutan == 0){
 	 			char *argv[] = {"mv", dp->d_name,"Kiper", NULL};
 	 			execv("/bin/mv", argv);
	  		}
	  		
	  		else{
	  			char *argv[] = {"sort","-r","-n", "Kiper", NULL};
 	 			execv("/bin/sort", argv);
			  }
	  		wait(&status_urutan);
		 }
		 
		 if(strstr(dp->d_name, "Bek") != NULL){
	 		urutan = fork();
	 
	 		if (urutan < 0){
	 			exit(EXIT_FAILURE);
	 		}
 	
 	 		if (urutan == 0){
 	 			char *argv[] = {"mv", dp->d_name, "Bek", NULL};
 	 			execv("/bin/mv", argv);
	  		}
	  		
	  		else{
	  			char *argv[] = {"sort","-r","-n", "Bek", NULL};
 	 			execv("/bin/sort", argv);
			  }	
			  
	  		wait(&status_urutan);
		 }
		 
		 if(strstr(dp->d_name, "Gelandang") != NULL){
	 		urutan = fork();
	 
	 		if (urutan < 0){
	 			exit(EXIT_FAILURE);
	 		}
 	
 	 		if (urutan == 0){
 	 			char *argv[] = {"mv", dp->d_name, "Gelandang", NULL};
 	 			execv("/bin/mv", argv);	
	  		}
	  		else{
	  			char *argv[] = {"sort","-r","-n", "Gelandang", NULL};
 	 			execv("/bin/sort", argv);
			  }
			  	
	  		wait(&status_urutan);
		 }
		 
		  if(strstr(dp->d_name, "Penyerang") != NULL){
	 		urutan = fork();
	 
	 		if (urutan < 0){
	 			exit(EXIT_FAILURE);
	 		}
 	
 	 		if (urutan == 0){
 	 			char *argv[] = {"mv", dp->d_name, "Penyerang", NULL};
 	 			execv("/bin/mv", argv);
	  		}
	  		
	  		else{
	  			char *argv[] = {"sort","-r","-n", "Penyerang", NULL};
 	 			execv("/bin/sort", argv);	
			  }
			  
	  		wait(&status_urutan);
		 }
	  }
}
	

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

int main(int argc, char **argv) {
    char *path = NULL;
    int hour, minute, second;

    if (argc != 5) {
        printf("Argumennya yang  benerrr\n");
        return 1;
    }

    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            printf("Jamnya yang benerrr\n");
            return 1;
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            printf("Menitnya yang benerrr\n");
            return 1;
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            printf("Detiknya yang benerrr\n");
            return 1;
        }
    }

    path = argv[4];

    if (access(path, F_OK) == -1) {
        printf("Error: File does not exist\n");
        return 1;
    }

    pid_t pid, pid2;
    pid = fork();

    if (pid == -1) {
        printf("Error: Fork failed\n");
        return 1;
    } else if (pid == 0) {
        time_t rawtime;
        struct tm * timeinfo;

        // while (1) {
        //     time(&rawtime);
        //     timeinfo = localtime(&rawtime);

        //     if ((hour == -1 || hour == timeinfo->tm_hour) &&
        //         (minute == -1 || minute == timeinfo->tm_min) &&
        //         (second == -1 || second == timeinfo->tm_sec)) {
                
        //         char *argv[] = {"bash", path, NULL};
        //         execv("/bin/bash", argv);
        //     }
        //     sleep(1);
        // }
        while (1) {
            // time_t now = time(NULL);
            // struct tm *tm_now = localtime(&now);
            time(&rawtime);
            timeinfo = localtime(&rawtime);

            if ((hour == -1 || timeinfo->tm_hour == hour) 
                && (minute == -1 || timeinfo->tm_min == minute) 
                && (second == -1 || timeinfo->tm_sec == second)) {
                
                pid2 = fork();

                if (pid2 == 0) {
                    char *argv[] = { "bash", path, NULL };
                    execvp("/bin/bash", argv);
                    // exit(0);
                }
            }
            sleep(1);
        }
    } else {
        exit(0);
    }

    return 0;
}
# **Praktikum Modul 2 Sistem Operasi**

Berikut adalah Repository dari Kelompok A10 untuk pengerjaan Praktikum Modul 2 Sistem Operasi. Dalam Repository ini terdapat Anggota Kelompok, Dokumentasi serta Penjelasan tiap soal, _Screenshot_ Output, dan Kendala yang Dialami.

# **Anggota Kelompok**

| Nama                      | NRP        | Kelas            |
| ------------------------- | ---------- | ---------------- |
| Anas Azhar                | 5025211043 | Sistem Operasi A |
| Dian Lies Seviona Dabukke | 5025211080 | Sistem Operasi A |
| Akmal Ariq Romadhon       | 5025211188 | Sistem Operasi A |

# **Dokumentasi dan Penjelasan Soal**

Berikut adalah dokumentasi yang berisi source code dari tiap soal dan penjelasan terkait perintah yang digunakan.

## **Soal Nomor 1**

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan [link download](https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq) dari drive kebun binatang tersebut

- **Soal Nomor 1A :** <br>
  Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

- **Soal Nomor 1B :** <br>
  Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

- **Soal Nomor 1C :** <br>
  Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama **HewanDarat, HewanAmphibi,** dan **HewanAir**. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

- **Soal Nomor 1D :** <br>
  Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

**Penyelesaian Soal Nomor 1 :**

- Pertama yaitu melakukan download file zip terlebih dahulu menggunakan kode di bawah ini:

```c++
    pid_t download_id;
    int status_download;

    download_id = fork();

    char link[] = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";

    if (download_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (download_id == 0) {
        char *argv[] = {"wget", link, "-qO", "binatang.zip", NULL};
        execv("/bin/wget", argv);
    }
    wait(&status_download);
```

    Menggunakan child process untuk menjalankan system `wget` dengan argumen "link" yang diisi dengan link drive, "-q0" yang digunakan untuk merubah nama file, dan argumen terakhir yang harus diisi dengan NULL.

- Selanjutnya yaitu melakukan unzip file hasil download tadi menggunakan kode di bawah ini:

```c++
    pid_t unzip_id;
    int status_unzip;

    unzip_id = fork();

    if (unzip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (unzip_id == 0) {
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }
    wait(&status_unzip);
```

    Seperti sebelumnya, menggunakan child process untuk menjalankan system `unzip` dengan argumen "-q" untuk menjalankan di latar belakang, dan "binatang.zip" yaitu file yang akan diunzip.

- Langkah selanjutnya yaitu memilih secara acak file gambar untuk melakukan shift penjagaan pada hewan tersebut menggunakan kode berikut:

```c++
    char basePath[] = ".";
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    char *path[11];

    if (!dir)
        return 0;

    int i = 0;
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            if (dp->d_type == DT_REG) {
                char *hewan  = dp->d_name;
                path[i] = hewan;
                i++;
            }
        }
    }
    int pilih = rand() % sizeof(*path);

    FILE *f = fopen("jaga.txt", "w");
    fprintf(f, "Penjagaan pada hewan %s", strtok(path[pilih], "."));
    fclose(f);

    closedir(dir);
```

- Berikutnya mengelompokkan hewan darat, air dan amphibi dan melakukan zip pada tiap-tiap foldernya...

```c++
    DIR *dirr = opendir(".");
    pid_t pilah_id;
    int status_pilah;

    while ((dp = readdir(dirr)) != NULL) {

        if (dp->d_type == DT_REG) {
            if (strstr(dp->d_name, "darat") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanDarat", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }

            if (strstr(dp->d_name, "amphibi") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAmphibi", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }

            if (strstr(dp->d_name, "air") != NULL) {
                pilah_id = fork();

                if (pilah_id < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilah_id == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAir", NULL};
                    execv("/bin/mv", argv);
                }
                wait(&status_pilah);
            }
        }
    }
    closedir(dirr);


    pid_t zip_id;
    int status_zip;

    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

    zip_id = fork();

    if (zip_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (zip_id == 0) {
        char *argv[] = {"zip", "-qr", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }
    wait(&status_zip);

```

- Dan terakhir yaitu melakukan delete setelah foolder-folder yang ada dicompress menjadi zip, menggunakan kode sebagai berikut:

```c++
    pid_t delete_id;
    int status_delete;

    delete_id = fork();

    if (delete_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (delete_id == 0) {
        char *argv[] = {"rm", "-r", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/rm", argv);
    }
    wait(&status_delete);
```

<br>

## **Soal Nomor 2**

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

- **Soal Nomor 2A :** <br>
  Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

- **Soal Nomor 2B :** <br>
  Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

- **Soal Nomor 2C :** <br>
  Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

- **Soal Nomor 2D :** <br>
  Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

- **Soal Nomor 2E :** <br>
  Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
  <br>

**Penyelesaian Soal 2 :**
<br>
Untuk menyelesaikan soal tersebut, code yang digunakan ialah sebagai berikut.

```c++
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){
    pid_t pid, sid;
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    FILE *fp;

    if(strcmp(argv[1], "-a") == 0){
        fp = fopen("killer.sh", "w");
        fprintf(fp, "killall -SIGKILL lukisan; rm killer.sh;\n");
        fclose(fp);
    }

    if(strcmp(argv[1], "-b") == 0){
        fp = fopen("killer.sh", "w");
        fprintf(fp, "kill -9 %d; rm killer.sh\n", (int)getpid());
        fclose(fp);
    }

    pid = fork();
    if(pid == 0){
        char *argvmod[] = {"chmod", "u+x", "killer.sh", NULL};
        execv("/bin/chmod", argvmod);
    }
    while((wait(NULL)) != pid);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        char ZipName[64], CurrentTime[30], link[50];
        pid_t cid;
        int status;

        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        char FolderName[64];
        strftime(FolderName, 64, "%Y-%m-%d_%H:%M:%S", tm);

        cid = fork();

        if(cid < 0){
            exit(EXIT_FAILURE);
        }

        if(cid == 0){
            if(fork()==0){
                char *argvmkdir[] = {"mkdir", "-p", FolderName, NULL};
                execv("/bin/mkdir", argvmkdir);
            }
            else{
                while((wait(&status)) > 0);
                for(int i=0 ; i<15 ; i++){
                    if(fork()==0){
                        if(chdir(FolderName) < 0){
                            exit(EXIT_FAILURE);
                        }
                        time_t t2 = time(NULL);
                        struct tm* p2 = localtime(&t2);
                        strftime(CurrentTime, 30, "%Y-%m-%d_%H:%M:%S", p2);
                        sprintf(link, "https://picsum.photos/%ld", (t2 % 1000) + 50);

                        char *argvdown[] = {"wget", link, "-O", CurrentTime, "-o", "/dev/null", NULL};
                        execv("/usr/bin/wget", argvdown);
                    }
                    sleep(5);
                }
                sprintf(ZipName, "%s.zip", FolderName);

                char *argvzip[] = {"zip", "-qrm", ZipName, FolderName, NULL};
                execv("/usr/bin/zip", argvzip);
            }
        }
        sleep(30);
    }
}

```

Dalam _code_ tersebut, terdapat beberapa bagian inti yang membuat _code_ dapat dijalankan dengan sempurna. Berikut adalah penjelasan beberapa bagiannya:

```c
pid_t pid, sid;
pid = fork();

if(pid < 0){
    exit(EXIT_FAILURE);
}

if(pid > 0){
    exit(EXIT_SUCCESS);
}

sid = setsid();

if(sid < 0){
    exit(EXIT_FAILURE);
}

```

Bagian ini berfungsi untuk menjadikan program sebagai daemon. Pertama, program melakukan fork untuk membuat _child process_. Jika proses fork gagal, program akan keluar dan mengembalikan nilai EXIT_FAILURE. Jika fork berhasil, proses utama akan keluar dengan nilai EXIT_SUCCESS dan proses anak akan melanjutkan eksekusi.

Setelah itu, proses anak akan memanggil fungsi `setsid()` untuk membuat _session_ baru dan menjadi pemimpin grup proses baru. Jika `setsid()` gagal, _child process_ akan keluar dengan nilai EXIT*FAILURE. Ini memastikan bahwa \_child process* berjalan pada _background_ dan tidak bergantung dari terminal yang memanggilnya.

```c
FILE *fp;

if(strcmp(argv[1], "-a") == 0){
    fp = fopen("killer.sh", "w");
    fprintf(fp, "killall -SIGKILL lukisan; rm killer.sh;\n");
    fclose(fp);
}

if(strcmp(argv[1], "-b") == 0){
    fp = fopen("killer.sh", "w");
    fprintf(fp, "kill -9 %d; rm killer.sh\n", (int)getpid());
    fclose(fp);
}
```

Bagian tersebut merupakan implementasi untuk soal nomor 2E, dimana terdapat 2 argumen yang dapat dimasukkan sebelum menjalankan program `lukisan.c`. Pada _if condition_ yang pertama atau argument `-a`, maka script akan mengirim sinyal SIGKILL ke semua proses yang memiliki nama "lukisan" dan kemudian menghapus script "killer.sh". Pada _if condition_ yang kedua atau argument `-b`, script akan mengirim sinyal SIGKILL ke proses yang memiliki PID sama dengan proses yang sedang berjalan dan kemudian menghapus script "killer.sh".

```c
    if(pid == 0){
        char *argvmod[] = {"chmod", "u+x", "killer.sh", NULL};
        execv("/bin/chmod", argvmod);
    }
```

Bagian tersebut berfungsi untuk mengubah _permission_ dari file `killer.sh` yang telah dibuat sebelumnya sehingga dapat dijalankan dengan baik.

```c
    while(1){
        char ZipName[64], CurrentTime[30], link[50];
        pid_t cid;
        int status;

        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        char FolderName[64];
        strftime(FolderName, 64, "%Y-%m-%d_%H:%M:%S", tm);

        cid = fork();

        if(cid < 0){
            exit(EXIT_FAILURE);
        }

        if(cid == 0){
            if(fork()==0){
                char *argvmkdir[] = {"mkdir", "-p", FolderName, NULL};
                execv("/bin/mkdir", argvmkdir);
            }
```

Secara garis besar, _code_ tersebut merupakan lanjutan dari _daemon process_ yang telah dijelaskan sebelumnya. Lalu, dalam code tersebut terdapat deklarasi variabel `ZipName`, `CurrentTime`, dan `link` yang akan digunakan selanjutnya. Variabel `t` dan `tm` berfungsi untuk mengambil waktu saat ini yang digunakan dalam fungsi `strftime(FolderName, 64, "%Y-%m-%d_%H:%M:%S", tm);` sebagai nama folder. Kemudian terdapat argumen `*argvmkdir[]` dan `execv` yang berfungsi dalam proses pembutan folder.

```c
else{
    while((wait(&status)) > 0);

    for(int i=0 ; i<15 ; i++){
        if(fork()==0){
            if(chdir(FolderName) < 0){
                exit(EXIT_FAILURE);
            }

            time_t t2 = time(NULL);
            struct tm* p2 = localtime(&t2);

            strftime(CurrentTime, 30, "%Y-%m-%d_%H:%M:%S", p2);

            sprintf(link, "https://picsum.photos/%ld", (t2 % 1000) + 50);

            char *argvdown[] = {"wget", link, "-O", CurrentTime, "-o", "/dev/null", NULL};
            execv("/usr/bin/wget", argvdown);}
                sleep(5);
    }
```

Bagian tersebut merupakan inti dari keseluruhan program. Secara garis besar, masih terdapat kemiripan dengan penjelasan sebelumnya. Program diawali dengan proses looping `for(int i=0 ; i<15 ; i++)` untuk memberikan batas 15x _download_. Kemudian terdapat variabel `t2` dan `p2` untuk mendapatkan waktu saat ini yang digunakan pada fungsi `strftime(CurrentTime, 30, "%Y-%m-%d_%H:%M:%S", p2);` . Kemudian terdapat fungsi `sprintf` yang berfungsi untuk memasukkan ketentuan atau parameter `"https://picsum.photos/%ld", (t2 % 1000) + 50` kedalam variabel `link` dan dilanjutkan dengan deklarasi variabel `*argvdown[]` dengan parameter `{"wget", link, "-O", CurrentTime, "-o", "/dev/null", NULL}` yang akan dieksekusi oleh `execv` sebagai proses download. Terakhir, terdapat perintah sleep(5) untuk memberi jeda 5 detik dari tiap gambar yang akan _di-download_.

```c
                sprintf(ZipName, "%s.zip", FolderName);

                char *argvzip[] = {"zip", "-qrm", ZipName, FolderName, NULL};
                execv("/usr/bin/zip", argvzip);
            }
        }
        sleep(30);
```

Setelah proses dilakukan, program dilanjutkan dengan fungsi sprintf yang berfungsi memindahkan isi dari variabel `FolderName` kedalam variabel `ZipName`. Kemudian dilakukan deklarasi argumen `*argvzip[]` dengan parameter `{"zip", "-qrm", ZipName, FolderName, NULL}` dan dijalankan dengan `execv` untuk proses zip. Program diakhiri dengan `sleep(30)` yang berfungsi untuk memberi jeda selama 30 detik dalam pembuatan folder.

## **Soal Nomor 3**

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut **hanya dengan 1 Program C** bernama **“filter.c”**

- **Soal Nomor 3A :** <br>
  Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

- **Soal Nomor 3B :** <br>
  Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.

- **Soal Nomor 3C :** <br>
  Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

- **Soal Nomor 3D :** <br>
  Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi\_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

**Penyelesaian Soal Nomor 3 :**\

````c++
    pid_t download;
    int status_download;
    char path[500] = "/home/players";
    download = fork();

    char link[] = "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";

    if (download < 0) {
        exit(EXIT_FAILURE);
    }

    if (download == 0) {
        char *argv[] = {"wget", link, "-qO", "players.zip", NULL};
        execv("/bin/wget", argv);
    }
	wait(&status_download);
    ```
Pada code ini, child process akan melakukan proses download berdasarkan link yang diberikan. Lalu -q berfungsi agar menonaktifkan output ke konsol atau terminal saat mengunduh file menggunakan perintah wget dan -O digunakan untuk menentukan nama file yang digunakan saat mengunduh file (pada proses ini diambil players.zip). Lalu  wait(&status_download)merupakan kode yang akan membuat proses parent menunggu proses child selesai. Variabel status_download akan menyimpan status keluaran dari proses child ketika proses tersebut selesai.
```c
    pid_t unzip;
    int status_unzip;

    unzip = fork();

    if (unzip < 0) {
        exit(EXIT_FAILURE);
    }

    if (unzip == 0) {
        char *argv[] = {"unzip", "-q", "players.zip", NULL};
        execv("/bin/unzip", argv);
    }
	wait(&status_unzip);
````

Mirip seperti proses download namun, perbedaannya terletak pada perintah yang diberikan yaitu unzip (ekstrak file players.zip).

```c
    // ==DELETE===================================
    pid_t delete_zip;
    int status_delete;

    delete_zip = fork();

    if (delete_zip< 0) {
        exit(EXIT_FAILURE);
    }

    if (delete_zip== 0) {
        char *argv[] = {"rm", "-r", "players.zip", NULL};
        execv("/bin/rm", argv);
    }
     wait(&status_delete);
```

Lalu pada proses delete ini juga mirip pada 2 proses sebelumnya namun, pada proses ini agar kita dapat menghapus file players.zip karena sudah diekstrak maka digunakan perintah rm dan -r agar file dan direktori terhapus secara rekursif.

```c
DIR *dir = opendir("players");
if (!dir) {
    perror("Directory open error");
    exit(1);
}

struct dirent *dp;
while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
            // Ignore . and .. directories
        continue;
    }

// Check if file name contains "ManUtd"
    if (strstr(dp->d_name, "ManUtd") == NULL) {
        char filepath[500];
        sprintf(filepath, "players/%s", dp->d_name);
        remove(filepath);
     }
 }
  closedir(dir);
```

struct dirent *dp; merupakan pointer untuk entri direktori dimana header file dirent.h untuk menangani operasi pada direktori lalu DIR *dir =opendir(“players”) dimana akan mengakses direktori players. Kemudian eksekusi selanjutnya digunakan untuk memeriksa apakah sebuah direktori berhasil dibuka atau tidak. Lalu, program akan membaca semua file yang ada di dalam direktori menggunakan fungsi readdir() dengan loop while. Setiap file akan disimpan ke dalam struktur dirent dp. Kode setelah while tersebut digunakan untuk memeriksa apakah direktori saat ini sedang memproses folder "." atau folder "..". Folder "." merujuk pada direktori saat ini, sedangkan folder ".." merujuk pada direktori induk dari direktori saat ini. Karena kedua folder tersebut bukan folder yang diperlukan, maka kode tersebut mengabaikannya dan melanjutkan proses pengecekan folder selanjutnya. Selanjutnya, program akan memeriksa apakah nama direktori dp sama dengan "ManUtd". Jika ya, sprintf() akan menggabungkan string "players" dengan nama file yang ditemukan oleh readdir(), yang disimpan dalam dp->d_name yaitu yang bukan ManUtd. Hasil dari penggabungan kedua string tersebut kemudian disimpan dalam buffer filepath. Setelah path file lengkap dibuat, fungsi remove() digunakan untuk menghapus file yang berada di path tersebut.

```c
pid_t folder_id;
int status_folder;

folder_id = fork();

if (folder_id < 0) {
    exit(EXIT_FAILURE);
}

if (folder_id == 0) {
   char *argv[] = {"touch", "Kiper", "Bek", "Gelandang", "Penyerang", NULL};
    execvp("touch", argv);
}
 wait(&status_folder);
```

Process child akan membuat file yang terdiri dari Kiper, Bek, Gelandang, Penyerang menggunakan fungsi “touch”. Karena kita menggunakan execvp maka, tidak perlu pakai Path.

```c
    DIR *dir2 = opendir(".");
    pid_t urutan;
    int status_urutan;

    while((dp = readdir(dir2)) != NULL){

	if(strstr(dp->d_name, "Kiper") != NULL){
	   urutan = fork();

	   if (urutan < 0){
	 	exit(EXIT_FAILURE);
	   }

 	   if (urutan == 0){
 	 	char *argv[] = {"mv", dp->d_name,"Kiper", NULL};
 	 	execv("/bin/mv", argv);
	   }

	   else{
	  	char *argv[] = {"sort","-r","-n", "Kiper", NULL};
 	 	execv("/bin/sort", argv);
	   }
	  	wait(&status_urutan);
	}
```

Perintah DIR \*dir2 = opendir(".") adalah perintah untuk membuka directory "." (directory saat ini) dan menyimpan pointer ke direktori tersebut pada variabel "dir2". Dalam hal ini, "." mengacu pada direktori di mana program filter.c dijalankan.
Selama program membaca semua file kita lakukan process dimana parent akan menjalankan pengurutan. Argumen “-r” dilakukan descending order dan “-n” pengurutan dilakukan secara numerik untuk pengurutan file players. Disini kita cari untuk bagian Kiper lalu child akan memindahkan isi dp->d_name kedalam file Kiper menggunakan perintah “mv”.

```c
	if(strstr(dp->d_name, "Bek") != NULL){
	urutan = fork();

	if (urutan < 0){
	exit(EXIT_FAILURE);
	}

 	if (urutan == 0){
 	char *argv[] = {"mv", dp->d_name, "Bek", NULL};
 	execv("/bin/mv", argv);
	}

	else{
	char *argv[] = {"sort","-r","-n", "Bek", NULL};
 	execv("/bin/sort", argv);
	}

	wait(&status_urutan);
	}

	if(strstr(dp->d_name, "Gelandang") != NULL){
	urutan = fork();

	if (urutan < 0){
	exit(EXIT_FAILURE);
	}

 	if (urutan == 0){
 	char *argv[] = {"mv", dp->d_name, "Gelandang", NULL};
 	execv("/bin/mv", argv);
	}

	else{
	char *argv[] = {"sort","-r","-n", "Gelandang", NULL};
 	execv("/bin/sort", argv);
	}

	wait(&status_urutan);
	}

	if(strstr(dp->d_name, "Penyerang") != NULL){
	urutan = fork();

	 if (urutan < 0){
	 exit(EXIT_FAILURE);
	 }

 	 if (urutan == 0){
 	char *argv[] = {"mv", dp->d_name, "Penyerang", NULL};
 	execv("/bin/mv", argv);
	}

	else{
	char *argv[] = {"sort","-r","-n", "Penyerang", NULL};
 	execv("/bin/sort", argv);
	}

	 wait(&status_urutan);
	 }
    }
```

Untuk process code diatas sama seperti pada pengurutan dan pemindahan file Kiper.

## **Soal Nomor 4**

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya. Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- **Soal Nomor 4A :** <br>
  Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

- **Soal Nomor 4B :** <br>
  Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

- **Soal Nomor 4C :** <br>
  Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

- **Soal Nomor 4D :** <br>
  Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

- **Soal Nomor 4E :** <br>
  Bonus poin apabila CPU state minimum

**Penyelesaian Soal Nomor 4 :**

- Hal pertama yang harus dilakukan yaitu memastikan input yang diberikan telah sesuai, yakni saat menjalankan program harus disertakan juga argumen yang meliputi 3 nilai jam, menit, dan detik serta 1 nilai yang berisi file bash yang akan dijalankan. Sehingga total argumen yang harus dikirimkan yaitu sejumlah 5 argumen.

```c++
    if (argc != 5) {
            printf("Argumennya yang  benerrr\n");
            return 1;
    }
```

- Selanjutnya pastikan 3 argumen yang merupakan jam, menit dan detik harus sesuai dengan format pada umumnya.

```c++
    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            printf("Jamnya yang benerrr\n");
            return 1;
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            printf("Menitnya yang benerrr\n");
            return 1;
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            printf("Detiknya yang benerrr\n");
            return 1;
        }
    }
```

Karena untuk mengisi 3 argumen diatas, format yang dimasukkan harus berupa '\*' atau 0-23 untuk jam, 0-59 untuk menit, dan 0-59 untuk detik.

- Langkah berikutnya yaitu menyiapkan file yang nantinya akan dirun menggunakan bash

```c++
    path = argv[4];

    if (access(path, F_OK) == -1) {
        printf("Error: File does not exist\n");
        return 1;
    }
```

- Dan terakhir yaitu pada proses corntabnya, dimana di sini akan menggunakan looping menyesuaikan dengan waktu yang ada.

```c++
    pid_t pid, pid2;
    pid = fork();

    if (pid == -1) {
        printf("Error: Fork failed\n");
        return 1;
    } else if (pid == 0) {
        time_t rawtime;
        struct tm * timeinfo;

        while (1) {
            time(&rawtime);
            timeinfo = localtime(&rawtime);

            if ((hour == -1 || timeinfo->tm_hour == hour)
                && (minute == -1 || timeinfo->tm_min == minute)
                && (second == -1 || timeinfo->tm_sec == second)) {

                pid2 = fork();

                if (pid2 == 0) {
                    char *argv[] = { "bash", path, NULL };
                    execvp("/bin/bash", argv);
                }
            }
            sleep(1);
        }
    } else {
        exit(0);
    }
```

# **_Screenshot_ Output**

Berikut adalah output dari tiap-tiap soal yang telah dikerjakan.

## **Soal 1**

Berikut adalah output dari soal nomor 1:

![soal1](https://gitlab.com/azharanas173/sisop-praktikum-modul-2-2023-bj-a10/uploads/2e37cf26c88314329bbda31a2f471d97/soal1.png)

## **Soal 2**

Berikut adalah output dari soal nomor 2: <br>
**Pembuatan Folder setiap 30 detik**

![soal2a](https://media.discordapp.net/attachments/1083730715113426985/1094185666469974086/Screenshot_from_2023-04-08_16-00-51.png?width=1095&height=616)

**Download File Setiap 5 Detik**

![soal2b](https://media.discordapp.net/attachments/1083730715113426985/1094185666696450118/Screenshot_from_2023-04-08_15-58-58.png?width=1095&height=616)

**Pembuatan _Executable Killer_**

![Soal2c](https://media.discordapp.net/attachments/1083730715113426985/1094185666952319036/Screenshot_from_2023-04-08_15-58-24.png?width=1095&height=616)

**Zip Yang Berisi 15 Gambar**

![soal2d](https://media.discordapp.net/attachments/1083730715113426985/1094185666251849769/Screenshot_from_2023-04-08_16-01-22.png?width=1095&height=616)

## **Soal 3**

Berikut adalah output dari soal nomor 3:<br>

**Pengunduhan Database**

![foto3a](https://user-images.githubusercontent.com/90541607/230719739-0464ff55-82de-4e6a-ad75-2c3864711d28.png)

**Ekstrak Folder**

![prak3a](https://user-images.githubusercontent.com/90541607/230719741-472c3e7f-3000-4cc1-afa8-d0ce43b865e7.png)

**Penghapusan Players.zip**

![prak3a](https://user-images.githubusercontent.com/90541607/230719742-af9323a9-8cec-4a05-b087-15560ded43cc.png)

**Penghapusan pemain yang bukan dari Manchester United**

![prak3b](https://user-images.githubusercontent.com/90541607/230719743-0a3b9b67-c42c-4115-970d-d31c1bfce0bc.png)
 
**Pembuatan File Masing-masing Posisi**   

![prak3c](https://user-images.githubusercontent.com/90541607/230719745-68fbe8ee-f800-4082-a5d1-25baf04d275d.png)

## **Soal 4**

Berikut adalah output dari soal nomor 4:
Karena disini digunakan file bash untuk log_encrypt seperti yang digunakan pada praktikum modul 1, maka output yang dihasilkan yaitu membuat file log yang terenkripsi.

![soal4.1](https://gitlab.com/azharanas173/sisop-praktikum-modul-2-2023-bj-a10/uploads/56146ec5f1aa60ecf48ec0602bec2adf/soal4.1.png)


![soal4.2](https://gitlab.com/azharanas173/sisop-praktikum-modul-2-2023-bj-a10/uploads/bb0e349ebbdab311a06fc0992f09212f/soal4.2.png)

# **Kendala Selama Pengerjaan**

Berikut merupakan beberapa kendala dan kesulitan yang kami alami dalam pengerjaan Praktikum Modul 2 Sistem Operasi. Kami berharap kendala ini dapat diberbaiki di Praktikum selanjutnya.

- Ubuntu yang beberapa kali mengalami masalah, seperti cursor mengalami _freeze_ secara tiba-tiba dan keyboard tidak merespon saat ditekan.
- Pada hari awal praktikum masih berusaha untuk memahami materi yang ada, sehingga cukup kesusahan dalam mengejar _deadline_.
- Kesusahan dalam implementasi kedalam code, padahal sudah mengerti perintah soal dan algoritma dari program yang ingin dibuat. Poin ini sebenarnya masih berhubungan dengan poin sebelumnya.
- Mulai sibuknya pekan perkuliahan sehingga waktu untuk mengerjakan praktikum semakin sempit.
- Code tereksekusi namun tidak memiliki hasil

# **End of The Line**

```c
#include <stdio.h>

int main(){
    printf("Thank you!");
}
```
